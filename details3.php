<?php include("header.php"); ?>

<section class="page-content page-content--office-details office--full">
    <h1 class="page-title">Leeuwarderweg 44 Heerenveen</h1>
    <img src="img/173294206.jpg" alt="" class="office-details-img">

    <h4>Omschrijving</h4>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi, cumque, nobis, possimus a rem aperiam suscipit culpa molestiae enim quis dolore facilis recusandae dolores illum mollitia accusamus autem odit voluptate fugiat illo.</p>

    <div class="office-availability">
        <section class="costs">
            <h4>Kosten</h4>
            <p>&euro;140/mnd</p>
        </section>
        <section class="availability">
            <h4>Beschikbaarheid</h4>
            <p>Vrij: 7 / 7</p>
        </section>
        <button class="btn btn-standalone" id="subscribeBtn" disabled="disabled">Inschrijven</button>
    </div>

    <p class="on-send-message on-send-message--success" id="subscribeSuccess">Bedankt voor uw interesse. We sturen zo spoedig mogelijk een email met meer&nbsp;informatie.</p>
</section>

<?php include("footer.php"); ?>
