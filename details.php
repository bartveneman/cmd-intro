<?php include("header.php"); ?>

<section class="page-content page-content--office-details">
    <h1 class="page-title">Straatnaam 123 Leeuwarden</h1>
    <img src="img/pand1.jpg" alt="" class="office-details-img">

    <h4>Omschrijving</h4>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam, doloremque, fugiat explicabo impedit ea laboriosam blanditiis eaque enim a repudiandae vero perferendis sed officia quam voluptates. Alias eveniet voluptates amet doloribus quia.</p>

    <div class="office-availability">
        <section class="costs">
            <h4>Kosten</h4>
            <p>&euro;60/mnd</p>
        </section>
        <section class="availability">
            <h4>Beschikbaarheid</h4>
            <p>Vrij: 4 / 10</p>
        </section>
        <button class="btn btn-standalone" id="subscribeBtn">Inschrijven</button>
    </div>

    <p class="on-send-message on-send-message--success" id="subscribeSuccess">Bedankt voor uw interesse. We sturen zo spoedig mogelijk een email met meer&nbsp;informatie.</p>
</section>

<?php include("footer.php"); ?>
