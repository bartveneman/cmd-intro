<?php include("header.php"); ?>

<section class="page-content page-content--login">
    <h1 class="page-title">Login</h1>

    <p class="on-send-message on-send-message--error" id="loginError">De combinatie van wachtwoord en gebruikersnaam is onjuist.</p>

    <form action="offices.php" id="loginForm">
        <label for="login-email">Emailadres</label>
        <input type="text" id="login-email" name="login-email" placeholder="iemand@nhl.nl">

        <label for="login-password">Wachtwoord</label>
        <input type="password" name="login-password" id="login-password">

        <input type="submit" class="btn btn-standalone btn--large" value="Inloggen">
    </form>
</section>

<?php include("footer.php"); ?>
