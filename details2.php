<?php include("header.php"); ?>

<section class="page-content page-content--office-details">
    <h1 class="page-title">Drachterstraatweg 67 Joure</h1>
    <img src="img/156021245.jpg" alt="" class="office-details-img">

    <h4>Omschrijving</h4>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem, blanditiis, dignissimos, quod sint rem architecto rerum ipsum maxime hic perferendis labore officiis dolorem tempore!</p>

    <div class="office-availability">
        <section class="costs">
            <h4>Kosten</h4>
            <p>&euro;120/mnd</p>
        </section>
        <section class="availability">
            <h4>Beschikbaarheid</h4>
            <p>Vrij: 5 / 6</p>
        </section>
        <button class="btn btn-standalone" id="subscribeBtn">Inschrijven</button>
    </div>

    <p class="on-send-message on-send-message--success" id="subscribeSuccess">Bedankt voor uw interesse. We sturen zo spoedig mogelijk een email met meer&nbsp;informatie.</p>
</section>

<?php include("footer.php"); ?>
