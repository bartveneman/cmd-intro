<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>The Workstation</title>

    <meta name="viewport" content="width=device-width, minimum-scale=1.0">

    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Merriweather:900,700,300">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
    
    <header class="page-header">
        <a href="offices.php" class="site-logo" id="site-logo">
            <img src="img/workstation.png" alt="">
        </a>

        <nav class="site-nav">
            <ol class="site-nav--list">
                <li class="site-nav--list-item">
                    <a href="offices.php" class="site-nav--link site-nav--link__offices">Bedrijfsruimte huren</a>
                </li>
                <li class="site-nav--list-item">
                    <a href="offices.php" class="site-nav--link site-nav--link__offices">Bedrijfsruimte plaatsen</a>
                </li>
                <li class="site-nav--list-item">
                    <a href="index.php" class="site-nav--link site-nav--link__login">Uitloggen</a>
                </li>
            </ol>
        </nav>
    </header>
