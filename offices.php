<?php include("header.php"); ?>

<section class="page-content page-content--home">
    <h1 class="page-title">Aanbod van panden</h1>

    <ol class="offices--list">

        <li class="offices--list-item">
            <a href="details.php" class="img">
                <img src="img/pand1.jpg" alt="">
            </a>
            <div class="details">
                <h3 class="office-streetname"><a href="details.php">Straatnaam 123 Leeuwarden</a></h3>

                <div class="office-meta">
                    <section>
                        <h4>Kosten</h4>
                        <p>&euro;60/mnd</p>
                    </section>
                    <section>
                        <h4>Beschikbaarheid</h4>
                        <p>Vrij: 4 / 10</p>
                    </section>
                    <a href="details.php">Bekijk details</a>
                </div>
            </div>
        </li>

        <li class="offices--list-item">
            <a href="details2.php" class="img">
                <img src="img/156021245.jpg" alt="">
            </a>
            <div class="details">
                <h3 class="office-streetname"><a href="details2.php">Drachterstraatweg 67 Joure</a></h3>

                <div class="office-meta">
                    <section>
                        <h4>Kosten</h4>
                        <p>&euro;120/mnd</p>
                    </section>
                    <section>
                        <h4>Beschikbaarheid</h4>
                        <p>Vrij: 5 / 6</p>
                    </section>
                    <a href="details2.php">Bekijk details</a>
                </div>
            </div>
        </li>

        <li class="offices--list-item office--full">
            <a href="details3.php" class="img">
                <img src="img/173294206.jpg" alt="">
            </a>
            <div class="details">
                <h3 class="office-streetname"><a href="details3.php">Leeuwarderweg 44 Heerenveen</a></h3>

                <div class="office-meta">
                    <section>
                        <h4>Kosten</h4>
                        <p>&euro;140/mnd</p>
                    </section>
                    <section>
                        <h4>Beschikbaarheid</h4>
                        <p>Vrij: 7 / 7 (Vol)</p>
                    </section>
                    <a href="details3.php">Bekijk details</a>
                </div>
            </div>
            <div style="clear:both"></div>
        </li>

    </ol>
</section>

<?php include("footer.php"); ?>
