<?php include("header.php"); ?>

<section class="page-content page-content--signup">
    <h1 class="page-title">Registreren</h1>

    <form action="">
        <label for="signup-email">Emailadres</label>
        <input type="email" id="signup-email" name="signup-email"  placeholder="iemand@nhl.nl" required>

        <label for="signup-password">Wachtwoord</label>
        <input type="password" name="signup-password" id="signup-password" required>

        <input type="submit" class="btn btn-standalone btn--large" value="Registreren">
    </form>
</section>

<?php include("footer.php"); ?>
