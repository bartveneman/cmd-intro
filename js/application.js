/*jslint browser: true*/
/*global lettering, fitText*/

/*warning: dirty try-catch strategy for event attachment*/

(function (window, document) {
    "use strict";

    var $ = function (el) {
            return document.getElementById(el);
        },

        handleSubscription = function () {
            var msgBox = $("subscribeSuccess").classList;
            msgBox.add("displayed");

            this.setAttribute("disabled", "disabled");
            this.firstChild.nodeValue = "Ingeschreven";

            // Make AJAX Call

            window.setTimeout(function () {
                msgBox.remove("displayed");
            }, 6000);
        },

        handleLoginError = function (event) {
            var email = $("login-email").value.trim(),
                pass = $("login-password").value.trim();

            if (email !== "" && pass !== "") {
                return true;
            } else {
                console.log("field(s) empty");

                var msgBox = $("loginError").classList;
                msgBox.add("displayed");

                window.setTimeout(function () {
                    msgBox.remove("displayed");
                }, 4000);

                event.preventDefault();
            }
        },

        attachEvents = function () {
            try {
                $("subscribeBtn").onclick = handleSubscription;
            } catch (e) {}

            try {
                $("loginForm").onsubmit = handleLoginError;
            } catch (e) {}
        };

    attachEvents();

}(this, document));
